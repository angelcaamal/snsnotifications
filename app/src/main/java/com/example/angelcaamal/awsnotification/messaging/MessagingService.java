package com.example.angelcaamal.awsnotification.messaging;

import android.util.Log;


import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;



public class MessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

            if (remoteMessage.getNotification() != null) {
                String text = remoteMessage.getNotification().getBody();
                Log.e("Text From Notifications", text);
            } else if (remoteMessage.getData() != null) {
                String message = remoteMessage.getData().toString();
                Log.e("Message in data",message);
            }

    }


}